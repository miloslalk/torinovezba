package dto;



public class UserDTO {
private int id;
//@Size(min = 2)
//@NotNull
//@NotEmpty
private String firstName;
//@Size(min = 2)
//@NotNull
//@NotEmpty
private String lastName;
//@NotNull
private int dateOfBirth;
//@NotNull
private String username;
//@NotNull
private String password;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public int getDateOfBirth() {
	return dateOfBirth;
}
public void setDateOfBirth(int dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public UserDTO(int id, String firstName, String lastName, int dateOfBirth, String username, String password) {
	super();
	this.id = id;
	this.firstName = firstName;
	this.lastName = lastName;
	this.dateOfBirth = dateOfBirth;
	this.username = username;
	this.password = password;
}
@Override
public String toString() {
	return "UserDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth
			+ ", username=" + username + ", password=" + password + ", getId()=" + getId() + ", getFirstName()="
			+ getFirstName() + ", getLastName()=" + getLastName() + ", getDateOfBirth()=" + getDateOfBirth()
			+ ", getUsername()=" + getUsername() + ", getPassword()=" + getPassword() + ", getClass()=" + getClass()
			+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
}
public UserDTO() {
	super();
}





}
